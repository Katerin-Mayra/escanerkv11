from django.contrib import admin
from apps.principal.models import Perfil,Direccion,Equipo,Relacion
# Register your models here.
admin.site.register(Perfil)
admin.site.register(Direccion)
admin.site.register(Equipo)
admin.site.register(Relacion)
