# Generated by Django 2.2 on 2021-02-17 13:16

from django.db import migrations, models
import django.db.models.deletion
import macaddress.fields


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Area',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_activo', models.BooleanField(default=True)),
                ('creado_en', models.DateTimeField(auto_now_add=True, null=True)),
                ('modificado_en', models.DateTimeField(auto_now=True, null=True)),
                ('detalle', models.CharField(blank=True, max_length=228)),
                ('orden', models.IntegerField(default=0)),
                ('nombre', models.CharField(max_length=128)),
                ('sigla', models.CharField(max_length=16, unique=True)),
                ('tipo', models.CharField(default='ÁREA', max_length=16)),
                ('etiqueta', models.CharField(blank=True, max_length=128, null=True)),
            ],
            options={
                'verbose_name': 'Área',
            },
        ),
        migrations.CreateModel(
            name='Areauno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(db_index=True, max_length=33, null=True)),
                ('direccion', models.CharField(max_length=33)),
                ('detalle', models.CharField(blank=True, max_length=50, unique=True)),
            ],
        ),
        migrations.CreateModel(
            name='Direccion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('ip', models.CharField(db_index=True, max_length=33, null=True, unique=True, verbose_name='IP Address')),
                ('puerta_enlace', models.CharField(default='0.0.0.0', max_length=33)),
                ('mascara', models.CharField(default='0.0.0.0', max_length=33)),
            ],
        ),
        migrations.CreateModel(
            name='Equipo',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre_host', models.CharField(max_length=50, unique=True)),
                ('sistema_operativo', models.CharField(choices=[('Windows XP', 'Windows XP'), ('Windows Vista', 'Windows Vista'), ('Windows 7', 'Windows 7'), ('Windows 8', 'Windows 8'), ('Windows 10', 'Windows 10'), ('Windows Server ', 'Windows Server '), ('Windows Server 2000 ', 'Windows Server 2000'), ('Windows Server 2003', 'Windows Server 2003'), ('Windows Server 2008', 'Windows Server 2008'), ('Windows Server 2012', 'Windows Server 2012'), ('Windows Server 2016', 'Windows Server 2016'), ('Windows Server 2019', 'Windows Server 2019'), ('Windows Small Business Server', 'Windows Small Business Server'), ('Windows Essential Business Server', 'Windows Essential Business Server'), ('Windows Home Server ', 'Windows Home Server '), ('Kali Linux', 'Kali Linux'), ('Mac OS', 'Mac OS'), ('Ubuntu', 'Ubuntu'), ('Ubuntu 5', 'Ubuntu 5'), ('Ubuntu 6', 'Ubuntu 6'), ('Ubuntu 7', 'Ubuntu 7'), ('Ubuntu 8', 'Ubuntu 8'), ('Ubuntu 9', 'Ubuntu 9'), ('Ubuntu 10', 'Ubuntu 10'), ('Ubuntu 11.04', 'Ubuntu 11.04'), ('Ubuntu 11.10', 'Ubuntu 11.10'), ('Ubuntu 12.04', 'Ubuntu 12.04'), ('Ubuntu 12.10', 'Ubuntu 12.10'), ('Ubuntu 13.04', 'Ubuntu 13.04'), ('Ubuntu 13.10', 'Ubuntu 13.10'), ('Ubuntu 14.04', 'Ubuntu 14.04'), ('Ubuntu 14.10', 'Ubuntu 14.10'), ('Ubuntu 15.04', 'Ubuntu 15.04'), ('Ubuntu 15.10', 'Ubuntu 15.10'), ('Ubuntu 16.04', 'Ubuntu 16.04'), ('Ubuntu 16.10', 'Ubuntu 16.10'), ('Ubuntu 17.04', 'Ubuntu 17.04'), ('Ubuntu 17.10', 'Ubuntu 17.10'), ('Ubuntu 18.04', 'Ubuntu 18.04'), ('Ubuntu 18.10', 'Ubuntu 18.10'), ('Ubuntu 19.04', 'Ubuntu 19.04'), ('Ubuntu 19.10', 'Ubuntu 19.10'), ('Ubuntu 20.04', 'Ubuntu 20.04'), ('Ubuntu 20.10', 'Ubuntu 20.10')], max_length=100)),
                ('mac', macaddress.fields.MACAddressField(blank=True, integer=True, max_length=50, null=True)),
                ('mac1', models.CharField(blank=True, max_length=50, null=True)),
                ('fecha', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Infraestructura',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('direccion', models.CharField(max_length=50)),
                ('detalle', models.CharField(blank=True, max_length=50, unique=True)),
                ('vlan', models.CharField(blank=True, default='Cliente', max_length=50)),
                ('fecha', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(max_length=50)),
                ('ci', models.CharField(max_length=10, unique=True)),
                ('apellido', models.CharField(max_length=50)),
                ('correo', models.CharField(blank=True, max_length=50, unique=True)),
                ('tipo', models.CharField(blank=True, default='Cliente', max_length=50)),
                ('fecha', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Red',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('numero', models.CharField(db_index=True, max_length=167, null=True)),
                ('puerta_enlace', models.CharField(max_length=33)),
                ('nombre', models.CharField(max_length=45)),
                ('mascara', models.CharField(default='255.255.255.0', max_length=33)),
                ('inicio', models.IntegerField(default=0, null=True)),
                ('fin', models.IntegerField(default=255, null=True)),
                ('fecha', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Secretaria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_activo', models.BooleanField(default=True)),
                ('creado_en', models.DateTimeField(auto_now_add=True, null=True)),
                ('modificado_en', models.DateTimeField(auto_now=True, null=True)),
                ('detalle', models.CharField(blank=True, max_length=228)),
                ('orden', models.IntegerField(default=0)),
                ('nombre', models.CharField(max_length=128)),
                ('sigla', models.CharField(max_length=16, unique=True)),
                ('tipo', models.CharField(max_length=16)),
                ('etiqueta', models.CharField(blank=True, max_length=128, null=True)),
            ],
            options={
                'verbose_name': 'Secretaría',
            },
        ),
        migrations.CreateModel(
            name='SecretariaArea',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('area', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Areauno')),
            ],
        ),
        migrations.CreateModel(
            name='SecretariaUnidad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FechaInicio', models.DateField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='Unidaduno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(db_index=True, max_length=33, null=True)),
                ('direccion', models.CharField(max_length=33)),
                ('detalle', models.CharField(blank=True, max_length=50, unique=True)),
                ('area', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Areauno')),
            ],
        ),
        migrations.CreateModel(
            name='Unidad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_activo', models.BooleanField(default=True)),
                ('creado_en', models.DateTimeField(auto_now_add=True, null=True)),
                ('modificado_en', models.DateTimeField(auto_now=True, null=True)),
                ('detalle', models.CharField(blank=True, max_length=228)),
                ('orden', models.IntegerField(default=0)),
                ('nombre', models.CharField(max_length=128)),
                ('sigla', models.CharField(max_length=16, unique=True)),
                ('tipo', models.CharField(max_length=16)),
                ('etiqueta', models.CharField(blank=True, max_length=128, null=True)),
                ('secretaria', models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='unidades', to='principal.Secretaria')),
            ],
            options={
                'verbose_name': 'Unidades',
            },
        ),
        migrations.CreateModel(
            name='Secretariauno',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('nombre', models.CharField(db_index=True, max_length=33, null=True)),
                ('direccion', models.CharField(max_length=33)),
                ('detalle', models.CharField(blank=True, max_length=50, unique=True)),
                ('area', models.ManyToManyField(through='principal.SecretariaArea', to='principal.Areauno')),
                ('unidad', models.ManyToManyField(through='principal.SecretariaUnidad', to='principal.Unidaduno')),
            ],
        ),
        migrations.AddField(
            model_name='secretariaunidad',
            name='secretaria',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Secretariauno'),
        ),
        migrations.AddField(
            model_name='secretariaunidad',
            name='unidad',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Unidaduno'),
        ),
        migrations.AddField(
            model_name='secretariaarea',
            name='secretaria',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Secretariauno'),
        ),
        migrations.CreateModel(
            name='RelacionPerfil',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_vigente', models.BooleanField(default=False)),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('FechaFinal', models.DateField(blank=True, null=True)),
                ('equipos', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Equipo')),
                ('perfil', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Perfil')),
            ],
        ),
        migrations.CreateModel(
            name='Relacion',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_vigente', models.BooleanField(default=False)),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('FechaFinal', models.DateField(blank=True, null=True)),
                ('direccion', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Direccion')),
                ('equipo', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Equipo')),
            ],
        ),
        migrations.CreateModel(
            name='RedUnidad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_vigente', models.BooleanField(default=True)),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('FechaFinal', models.DateField(blank=True, null=True)),
                ('red', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Red')),
                ('unidad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Unidad')),
            ],
        ),
        migrations.CreateModel(
            name='RedSecretaria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_vigente', models.BooleanField(default=True)),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('FechaFinal', models.DateField(blank=True, null=True)),
                ('red', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Red')),
                ('secretaria', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Secretaria')),
            ],
        ),
        migrations.CreateModel(
            name='RedArea',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('esta_vigente', models.BooleanField(default=True)),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('FechaFinal', models.DateField(blank=True, null=True)),
                ('area', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Area')),
                ('red', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Red')),
            ],
        ),
        migrations.AddField(
            model_name='red',
            name='area',
            field=models.ManyToManyField(through='principal.RedArea', to='principal.Area'),
        ),
        migrations.AddField(
            model_name='red',
            name='secretaria',
            field=models.ManyToManyField(through='principal.RedSecretaria', to='principal.Secretaria'),
        ),
        migrations.AddField(
            model_name='red',
            name='unidad',
            field=models.ManyToManyField(through='principal.RedUnidad', to='principal.Unidad'),
        ),
        migrations.CreateModel(
            name='InfraestructuraUnidad',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('infraestructura', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Infraestructura')),
                ('unidad', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Unidad')),
            ],
        ),
        migrations.CreateModel(
            name='InfraestructuraSecretaria',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('infraestructura', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Infraestructura')),
                ('secretaria', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Secretaria')),
            ],
        ),
        migrations.CreateModel(
            name='InfraestructuraArea',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('FechaInicio', models.DateField(auto_now_add=True)),
                ('area', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Area')),
                ('infraestructura', models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Infraestructura')),
            ],
        ),
        migrations.AddField(
            model_name='infraestructura',
            name='area',
            field=models.ManyToManyField(through='principal.InfraestructuraArea', to='principal.Area'),
        ),
        migrations.AddField(
            model_name='infraestructura',
            name='secretaria',
            field=models.ManyToManyField(through='principal.InfraestructuraSecretaria', to='principal.Secretaria'),
        ),
        migrations.AddField(
            model_name='infraestructura',
            name='unidad',
            field=models.ManyToManyField(through='principal.InfraestructuraUnidad', to='principal.Unidad'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='perfil1',
            field=models.ManyToManyField(through='principal.RelacionPerfil', to='principal.Perfil'),
        ),
        migrations.AddField(
            model_name='equipo',
            name='relacion1',
            field=models.ManyToManyField(through='principal.Relacion', to='principal.Direccion'),
        ),
        migrations.AddField(
            model_name='direccion',
            name='red',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, to='principal.Red'),
        ),
        migrations.AddField(
            model_name='area',
            name='secretaria',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='areas', to='principal.Secretaria'),
        ),
        migrations.AddField(
            model_name='area',
            name='unidad',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='areas', to='principal.Unidad'),
        ),
    ]
